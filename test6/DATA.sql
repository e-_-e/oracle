DECLARE
    v_goods_id NUMBER;
    v_goods_name VARCHAR2(100);
    v_price NUMBER;
    v_production_date DATE;
    v_sale_id NUMBER;
    v_sale_time DATE;
    v_quantity NUMBER;
    v_amount NUMBER;
    v_customer_id NUMBER;
    v_customer_name VARCHAR2(100);
    v_phone VARCHAR2(20);
BEGIN
    FOR i IN 1..100000 LOOP
        -- 生成商品数据
        v_goods_id := i;
        v_goods_name := '商品' || i;
        v_price := ROUND(DBMS_RANDOM.VALUE(1, 1000), 2);
        v_production_date := TRUNC(SYSDATE) - DBMS_RANDOM.VALUE(0, 365);

        -- 插入商品数据
        INSERT INTO GOODS (GOODS_ID, GOODS_NAME, PRICE, PRODUCTION_DATE)
        VALUES (v_goods_id, v_goods_name, v_price, v_production_date);

        -- 生成销售记录数据
        v_sale_id := i;
        v_sale_time := SYSDATE - DBMS_RANDOM.VALUE(0, 365);
        v_quantity := ROUND(DBMS_RANDOM.VALUE(1, 10));
        v_amount := v_quantity * v_price;

        -- 插入销售记录数据
        INSERT INTO SALE_RECORD (SALE_ID, SALE_TIME, QUANTITY, AMOUNT)
        VALUES (v_sale_id, v_sale_time, v_quantity, v_amount);

        -- 生成客户数据
        v_customer_id := i;
        v_customer_name := '客户' || i;
        v_phone := '手机号' || i;

        -- 插入客户数据
        INSERT INTO CUSTOMER (CUSTOMER_ID, CUSTOMER_NAME, PHONE)
        VALUES (v_customer_id, v_customer_name, v_phone);

        -- 生成库存数据
        v_quantity := ROUND(DBMS_RANDOM.VALUE(10, 100));
        v_amount := v_quantity * v_price;

        -- 插入库存数据
        INSERT INTO STOCK (GOODS_ID, QUANTITY, AMOUNT)
        VALUES (v_goods_id, v_quantity, v_amount);
    END LOOP;
    
    COMMIT;
END;
/
