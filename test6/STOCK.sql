-- 创建库存表
CREATE TABLE STOCK (
GOODS_ID NUMBER(10) PRIMARY KEY,
STOCK_QUANTITY NUMBER(10),
STOCK_AMOUNT NUMBER(10, 2),
FOREIGN KEY (GOODS_ID) REFERENCES GOODS(GOODS_ID)
) TABLESPACE SALE_DATA;

