-- 创建用户及权限分配
CREATE USER admin IDENTIFIED BY admin_password;
GRANT CONNECT, RESOURCE, DBA TO admin;

CREATE USER sale_user IDENTIFIED BY sale_password;
GRANT CONNECT, RESOURCE TO sale_user;
