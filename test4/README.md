## 实验4：PL/SQL语言打印杨辉三角
姓名：付晓宇 学号：202010414105

### 实验目的

掌握Oracle PL/SQL语言以及存储过程的编写。
### 实验内容
1. 杨辉三角源代码。

```
set serveroutput on;
declare
type t_number is varray (100) of integer not null; --数组
i integer;
j integer;
spaces varchar2(30) :='   '; --三个空格，用于打印时分隔数字
N integer := 9; -- 一共打印9行数字
rowArray t_number := t_number();
begin
    dbms_output.put_line('1'); --先打印第1行
    dbms_output.put(rpad(1,9,' '));--先打印第2行
    dbms_output.put(rpad(1,9,' '));--打印第一个1
    dbms_output.put_line(''); --打印换行
    --初始化数组数据
    for i in 1 .. N loop
        rowArray.extend;
    end loop;
    rowArray(1):=1;
    rowArray(2):=1;    
    for i in 3 .. N --打印每行，从第3行起
    loop
        rowArray(i):=1;    
        j:=i-1;
            --准备第j行的数组数据，第j行数据有j个数字，第1和第j个数字为1
            --这里从第j-1个数字循环到第2个数字，顺序是从右到左
        while j>1 
        loop
            rowArray(j):=rowArray(j)+rowArray(j-1);
            j:=j-1;
        end loop;
            --打印第i行
        for j in 1 .. i
        loop
            dbms_output.put(rpad(rowArray(j),9,' '));--打印第一个1
        end loop;
        dbms_output.put_line(''); --打印换行
    end loop;
END;
/

```
2. 将源代码转为hr用户下的一个存储过程Procedure，名称为YHTriange，存储起来。存储过程接受行数N作为参数。

- 2.1 代码完成
```
create or replace procedure yhtriange (n in integer) is
  type t_number is varray(100) of integer not null; --数组
  i integer;
  j integer;
  spaces varchar2(30) := '   ';--三个空格，用于打印时分隔数字
  rowarray t_number := t_number();
begin
  dbms_output.put_line('1'); --先打印第1行
  dbms_output.put(rpad(1, 9, ' ')); --先打印第2行
  dbms_output.put(rpad(1, 9, ' ')); --打印第一个1
  dbms_output.put_line(''); --打印换行

  --初始化数组数据
  for i in 1 .. n loop
    rowarray.extend;
  end loop;
  
  rowarray(1) := 1;
  rowarray(2) := 1;

  for i in 3 .. n --打印每行，从第3行起
  loop
    rowarray(i) := 1;
    j := i - 1;
    
        --准备第j行的数组数据，第j行数据有j个数字，第1和第j个数字为1
        --这里从第j-1个数字循环到第2个数字，顺序是从右到左
    while j > 1 loop
      rowarray(j) := rowarray(j) + rowarray(j - 1);
      j := j - 1;
    end loop;
    
    --打印第i行
    for j in 1 .. i loop
      dbms_output.put(rpad(rowarray(j), 9, ' ')); --打印第一个1
    end loop;
  
    dbms_output.put_line(''); --打印换行
  end loop;

end;
/

```
![存储过程创建](./存储过程创建.jpg)
![存储目录展示](./存储过程目录展示.jpg)
- 2.2 工具完成

步骤：打开oracle sql Developer，找到hr表，往下翻到过程，右键新建过程，修改过程名字，添加过程参数。在begin end里面添加杨辉三角begin end的代码
![工具完成](./工具完成.jpg)
3. 运行这个存储过程即可以打印出N行杨辉三角。
``` 
exec yhtriange(9);
```
![输入9](./输入9.jpg)
``` 
exec yhtriange(20);
```
![输入20](./输入20.jpg)

### 总结
在本次实验中，我通过将源代码转换为hr用户下的一个存储过程，并成功地运行了这个存储过程，打印出了N行杨辉三角，深入了解了存储过程的创建和使用。同时，我还学会了如何编写创建存储过程的SQL语句，这对于以后的数据库开发和管理工作都非常有用。存储过程的重要性和优势在于它可以提高数据库的性能和安全性，减少重复的代码编写，提高开发效率。因此，我将继续学习和掌握存储过程的相关知识，为以后的数据库开发和管理工作做好准备。

