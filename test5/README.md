# 实验5：包，过程，函数的用法

201810414105 付晓宇

## 实验目的

- 了解PL/SQL语言结构
- 了解PL/SQL变量和常量的声明和使用方法
- 学习包，过程，函数的用法。

## 实验内容

- 以hr用户登录

1. 创建一个包(Package)，包名是MyPack。
2. 在MyPack中创建一个函数Get_SalaryAmount,输入的参数是部门ID，通过查询员工表，统计每个部门的salay工资总额。
3. 在MyPack中创建一个过程GET_EMPLOYEES,输入参数是员工ID，在过程中使用游标，通过查询员工表，递归查询某个员工及其所有下属，子下属员工。
Oracle递归查询的语句格式是：

```sql
SELECT LEVEL,EMPLOYEE_ID,FIRST_NAME,MANAGER_ID FROM employees 
START WITH EMPLOYEE_ID = V_EMPLOYEE_ID 
CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID
```



## 脚本代码

```sql
/*
本实验以实验4为基础。
包MyPack中有：
一个函数:Get_SalaryAmount(V_DEPARTMENT_ID NUMBER)，
一个过程:Get_Employees(V_EMPLOYEE_ID NUMBER)
*/
SQL> create or replace PACKAGE MyPack IS
FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER;
PROCEDURE Get_Employees(V_EMPLOYEE_ID NUMBER);
END MyPack;

程序包已创建。

SQL> create or replace PACKAGE BODY MyPack IS
FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER
AS
    N NUMBER(20,2); --注意，订单ORDERS.TRADE_RECEIVABLE的类型是NUMBER(8,2),汇总之后，数据要大得多。
    BEGIN
    SELECT SUM(salary) into N  FROM EMPLOYEES E
    WHERE E.DEPARTMENT_ID =V_DEPARTMENT_ID;
    RETURN N;
    END;

PROCEDURE GET_EMPLOYEES(V_EMPLOYEE_ID NUMBER)
AS
    LEFTSPACE VARCHAR(2000);
    begin
    --通过LEVEL判断递归的级别
    LEFTSPACE:=' ';
    --使用游标
    for v in
        (SELECT LEVEL,EMPLOYEE_ID, FIRST_NAME,MANAGER_ID FROM employees
        START WITH EMPLOYEE_ID = V_EMPLOYEE_ID
        CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID)
    LOOP
        DBMS_OUTPUT.PUT_LINE(LPAD(LEFTSPACE,(V.LEVEL-1)*4,' ')||
                            V.EMPLOYEE_ID||' '||v.FIRST_NAME);
    END LOOP;
    END;
END MyPack;
/

程序包体已创建。
```

## 测试

```sh

函数Get_SalaryAmount()测试：
SQL> select department_id,department_name,MyPack.Get_SalaryAmount(department_id) AS salary_total from departments;

DEPARTMENT_ID DEPARTMENT_NAME		     SALARY_TOTAL
------------- ------------------------------ ------------
	   10 Administration			     4400
	   20 Marketing 			    19000
	   30 Purchasing			    24900
	   40 Human Resources			     6500
	   50 Shipping				   156400
	   60 IT				    28800
	   70 Public Relations			    10000
	   80 Sales				   304500
	   90 Executive 			    58000
	  100 Finance				    51608
	  110 Accounting			    20308

DEPARTMENT_ID DEPARTMENT_NAME		     SALARY_TOTAL
------------- ------------------------------ ------------
	  120 Treasury
	  130 Corporate Tax
	  140 Control And Credit
	  150 Shareholder Services
	  160 Benefits
	  170 Manufacturing
	  180 Construction
	  190 Contracting
	  200 Operations
	  210 IT Support
	  220 NOC

DEPARTMENT_ID DEPARTMENT_NAME		     SALARY_TOTAL
------------- ------------------------------ ------------
	  230 IT Helpdesk
	  240 Government Sales
	  250 Retail Sales
	  260 Recruiting
	  270 Payroll

已选择 27 行。


过程Get_Employees()测试：
SQL> set serveroutput on
DECLARE
V_EMPLOYEE_ID NUMBER;    
BEGIN
V_EMPLOYEE_ID := 101;
MYPACK.Get_Employees (  V_EMPLOYEE_ID => V_EMPLOYEE_ID) ;    
END;
/SQL> 
101 Neena
108 Nancy
109 Daniel
110 John
111 Ismael
112 Jose Manuel
113 Luis
200 Jennifer
203 Susan
204 Hermann
205 Shelley
206 William

PL/SQL 过程已成功完成。
```

## 实验总结与体会

1.这次实验中，我学习了PL/SQL语言的结构以及变量和常量的声明和使用方法。
2.了解了包、过程和函数的用法，并在实验中进行了实际的编程练习。
4.通过这次实验，我深入了解了PL/SQL语言的特点和用途。我发现PL/SQL是一种强大的编程语言，特别适合用于处理和管理Oracle数据库。它具有丰富的语法和功能，可以进行复杂的数据操作和逻辑控制。我编写了一些简单的PL/SQL代码，包括变量和常量的声明和使用，以及过程和函数的定义和调用。通过这些练习，我熟悉了PL/SQL的语法和编程风格，并学会了如何编写结构清晰、可读性强的代码。
5.总的来说，这次实验使我对PL/SQL语言有了更深入的了解。我掌握了PL/SQL的基本语法和特性，并学会了如何利用其强大的功能来处理和管理Oracle数据库。


